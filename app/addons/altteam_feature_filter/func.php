<?php
/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/


use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_altteam_feature_filter_get_product_features($fields, $join, &$condition, &$params)
{
    if(!empty($params['feature_filter'])){
        $condition .= db_quote(' AND ?:product_features_descriptions.description LIKE ?l', '%' . trim($params['feature_filter']) . '%');
    }
}