<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
 ****************************************************************************/

use Tygh\Registry;
use Tygh\Tygh;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'update') {
    if (!empty($_REQUEST['feature_filter'])) {

        fn_print_r($_REQUEST);
        $product_data = Registry::get('view')->getTemplateVars('product_data');

        list($product_features, $features_search) = fn_get_paginated_product_features(
            array(
                'product_id' => $product_data['product_id'],
                'feature_filter' => $_REQUEST['feature_filter']
            ),
            $auth,
            $product_data,
            DESCR_SL
        );

        $taxes = fn_get_taxes();

        if(empty($product_features)){
            $product_features[] = [];
            $features_search[] = [];
            Tygh::$app['view']->assign([
                'product_features' => $product_features,
                'features_search' => $features_search
            ]);
        }else{
            Tygh::$app['view']->assign([
                'product_features' => $product_features,
                'features_search' => $features_search
            ]);
        }
    
       
    }
}
