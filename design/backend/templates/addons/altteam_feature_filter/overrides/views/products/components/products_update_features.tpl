<div id="content_features" class="hidden">

<div class="control-group">
<label class="control-label" for="feature_filter">{__("filter")}</label>
    <div class="controls">
        <input name="feature_filter"class="select2-search__field user-success" type='text' id="feature_filter_input" />
        {include file="buttons/button.tpl" but_role="action" but_text=__("search") but_id="feature_filter_submit"}
    </div>
</div><hr />

{if $product_features}

{include file="common/pagination.tpl" search=$features_search div_id="product_features_pagination_`$product_id`" current_url="products.get_features?product_id=`$product_id`&items_per_page=`$features_search.items_per_page`&feature_filter=`$features_search.feature_filter`"|fn_url disable_history=true}

<fieldset>
    {include file="views/products/components/product_assign_features.tpl" product_features=$product_features}
</fieldset>

{include file="common/pagination.tpl" search=$features_search div_id="product_features_pagination_`$product_id`" current_url="products.get_features?product_id=`$product_id`&items_per_page=`$features_search.items_per_page`&feature_filter=`$features_search.feature_filter`"|fn_url disable_history=true}

{else}
<p class="no-items">{__("no_items")}</p>
{/if}
<script>
$(document).keypress(function (e) {
    if (e.which == 13) {
            $("#feature_filter_submit").click();
    }
});
$("#feature_filter_submit").click(function(){
    $.ceAjax('request', fn_url("products.update"), {
        method: 'GET',
        data: {
            product_id: "{$product_id}",
            feature_filter: $("#feature_filter_input").val(),
        },
        result_ids:'product_features_pagination_{$product_id},content_features',
        callback: function(){
            $("#feature_0").addClass('hidden');
            return;
        },
    });
});

</script>
</div>